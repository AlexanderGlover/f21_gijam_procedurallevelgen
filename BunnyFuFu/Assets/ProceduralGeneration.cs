using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralGeneration : MonoBehaviour
{
    public struct MapModule
    {
        public int[,] contents;
        public bool[] entrances;
    }

    public Sprite[] tileSprites;
    public float[] tileTypeWeightings = {100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
    public float spriteSize = 1.0f;

    private MapModule spawnModule;
    private MapModule[] baseModules;

    //Use grid height/width to shrink 10/10 max size
    public int GridHeight = 4;
    public int GridWidth = 4;
    private MapModule[,] mGridMap = new MapModule[10, 10];

    const int spritesPerTileWidth = 10;

    private bool pathVerified = false;

    private float closestDistanceToGoal = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        PopulateTiles();
    }

    private void PopulateTiles()
    { 
        for(int i = 0; i < GridWidth; i++)
        {
            for (int j = 0; j < GridHeight; j++)
            {
                GenerateTile(i, j);
            }
        }

        int attempts = 0;
        while(!pathVerified && attempts < 15)
        {
            VerifyPath();
            attempts++;
        }

        PlaceStartAndEnd();

        Vector3 startPos = new Vector3(0.0f, 0.0f, 0.0f);
        float tileSize = spriteSize * spritesPerTileWidth;
        for (int i = 0; i < GridWidth; i++)
        {
            for (int j = 0; j < GridHeight; j++)
            {
                Vector3 position = startPos + new Vector3(tileSize * i, tileSize * j, 0.0f);
                PrintTile(mGridMap[i, j], position);
            }
        }
    }

    private void PlaceStartAndEnd()
    {
        int randx = (int)Random.Range(1, 9);
        int randy = (int)Random.Range(1, 9);

        mGridMap[0, 0].contents[randx, randy] = 9;

        randx = (int)Random.Range(1, 9);
        randy = (int)Random.Range(1, 9);

        mGridMap[GridWidth-1, GridHeight-1].contents[randx, randy] = 9;
    }

    private void PrintTile(MapModule tile, Vector3 position)
    {
        GameObject tileObj = new GameObject("Tile");
        tileObj.transform.position = position;
        int tileLength = tile.contents.GetLength(0);

        for (int i = 0; i < tileLength; i++)
        {
            for (int j = 0; j < tileLength; j++)
            {
                PrintSprite(tile, tileObj, i, j);
            }
        }        
    }

    private void PrintSprite(MapModule tile, GameObject parent, int xCoord, int yCoord)
    {
        GameObject subTileObj = new GameObject("SubTile");
        subTileObj.transform.parent = parent.transform;

        SpriteRenderer rend = subTileObj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        rend.sprite = tileSprites[tile.contents[xCoord, yCoord]];

        subTileObj.transform.localPosition = new Vector3(xCoord, yCoord, 0.0f) * spriteSize;
        subTileObj.transform.localScale = new Vector3(0.7955f, 0.7818f, 1.0f);
    }

    private void GenerateTile(int xCoord, int yCoord)
    {
        MapModule tile = new MapModule();
        tile.entrances = CheckTileRequirements(xCoord, yCoord);
        tile.contents = new int[spritesPerTileWidth, spritesPerTileWidth];

        tile = GenerateBoundary(tile);
        tile = GenerateInternals(tile);
        mGridMap[xCoord, yCoord] = tile;
    }

    private MapModule GenerateBoundary(MapModule tile)
    {
        int tileLength = tile.contents.GetLength(0);
        // Left Edge
        for (int i = 0; i < tileLength; i++)
        {
            if((i == 4 || i == 5) && tile.entrances[0])
            {
                tile.contents[0, i] = GetRandomWalkableTile();
            }
            else
            {
                tile.contents[0, i] = 8;
            }
        }

        // Right Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[1])
            {
                tile.contents[tileLength - 1, i] = GetRandomWalkableTile();
            }
            else
            {
                tile.contents[tileLength - 1, i] = 8;
            }
        }

        // Top Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[2])
            {
                tile.contents[i, 0] = GetRandomWalkableTile();
            }
            else
            {
                tile.contents[i, 0] = 8;
            }
        }

        // Bottom Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[3])
            {
                tile.contents[i, tileLength - 1] = GetRandomWalkableTile();
            }
            else
            {
                tile.contents[i, tileLength - 1] = 8;
            }
        }

        return tile;
    }
    private MapModule GenerateInternals(MapModule tile)
    {
        int tileLength = tile.contents.GetLength(0);
        for (int i = 1; i < tileLength -1; i++)
        {
            for (int j = 1; j < tileLength - 1; j++)
            {
                tile.contents[i, j] = GetRandomTileWeighted();
            }
        }

        return tile;
    }

    private int GetRandomTileWeighted()
    {
        float totalWeighting = SumArray(tileTypeWeightings);

        float rand = Random.Range(0, 100)/100.0f;

        int index = 0;
        float counter = 0.0f;
        foreach(float weighting in tileTypeWeightings)
        {
            if((counter + weighting)/totalWeighting > rand)
            {
                return(index);
            }
            else
            {
                counter += weighting;
                index++;
            }
        }
        return (0);
    }

    public float SumArray(float[] toBeSummed)
    {
        float sum = 0;
        foreach (float item in toBeSummed)
        {
            sum += item;
        }
        return sum;
    }

    private int GetRandomWalkableTile()
    {
        return Random.Range(0, 5);
    }

    private bool[] CheckTileRequirements(int xCoord, int yCoord)
    {
        bool[] results = {false, false, false, false };

        //Left
        if(xCoord != 0 && mGridMap[xCoord - 1, yCoord].entrances[1])
        {
            results[0] = true;
        }
        
        //Bottom
        if(yCoord != 0 && mGridMap[xCoord, yCoord - 1].entrances[3])
        {
            results[2] = true;
        }

        //Right
        if(xCoord != (GridWidth-1) && Random.Range(0,100) < 40)
        {
            results[1] = true;
        }

        //Top
        if(yCoord != (GridHeight-1) && Random.Range(1, 100) < 40)
        {
            results[3] = true;
        }

        return results;
    }

    private void VerifyPath()
    {
        Vector2 startPoint = new Vector2(0.0f, 0.0f);
        Vector2 goalPoint = new Vector2(GridWidth-1, GridHeight-1);
        closestDistanceToGoal = 100.0f;

        Vector2 closestPoint = NextStep(startPoint, 0, startPoint, goalPoint);
        if(!pathVerified)
        {
            int index = 0;
            foreach(bool enterance in mGridMap[(int)closestPoint.x, (int)closestPoint.y].entrances)
            {
                if(!(closestPoint.x == 0.0f && index == 0) && !(closestPoint.y == 0.0f && index == 2) && !(closestPoint.x == (float)(GridWidth - 1) && index == 1) && !(closestPoint.y == (float)(GridHeight-1) && index == 3)  && !enterance)
                {
                    mGridMap[(int)closestPoint.x, (int)closestPoint.y].entrances[index] = true;

                    Vector2 direction;
                    int neighborEntrance = 0;
                    switch (index)
                    {
                        case 0:
                            direction = new Vector2(-1, 0);
                            neighborEntrance = 1;
                            break;
                        case 1:
                            direction = new Vector2(1, 0);
                            neighborEntrance = 0;
                            break;
                        case 2:
                            direction = new Vector2(0, -1);
                            neighborEntrance = 3;
                            break;
                        default:
                            direction = new Vector2(0, 1);
                            neighborEntrance = 2;
                            break;
                    }
                    MapModule neighbor = mGridMap[(int)closestPoint.x + (int)direction.x, (int)closestPoint.y + (int)direction.y];

                    neighbor.entrances[neighborEntrance] = true;
                    GenerateBoundary(neighbor);
                }
                GenerateBoundary(mGridMap[(int)closestPoint.x, (int)closestPoint.y]);
                index++;
            }
        }
    }

    private Vector2 NextStep(Vector2 startPoint, int pathLength, Vector2 closestTile, Vector2 goalPoint)
    {
        if(pathLength > 15)
        {
            return(closestTile);
        }

        if(startPoint == goalPoint)
        {
            pathVerified = true;
            return(closestTile);
        }

        float distanceToGoal = (goalPoint.x - startPoint.x) + (goalPoint.y - startPoint.y);
        if(distanceToGoal < closestDistanceToGoal)
        {
            closestTile = startPoint;
            closestDistanceToGoal = distanceToGoal;
        }

        int index = 0;
        foreach(bool exit in mGridMap[(int)startPoint.x, (int)startPoint.y].entrances)
        {
            if(exit)
            {
                Vector2 direction;
                switch (index)
                {
                    case 0:
                        direction = new Vector2(-1, 0);
                        break;
                    case 1:
                        direction = new Vector2(1, 0 );
                        break;
                    case 2:
                        direction = new Vector2(0, -1);
                        break;
                    default:
                        direction = new Vector2(0, 1);
                        break;
                }

                closestTile = NextStep(startPoint + direction, pathLength + 1, closestTile, goalPoint);
            }
            index++;
        }

        return closestTile;
    }
}
