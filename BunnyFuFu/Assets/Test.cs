using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    //Variables
    public Sprite[] tileSprites;
    public float[] tileWeightings;

    private float spriteSize = 2.0f;

    //0: Left, 1: Right, 2: Bottom, 3: Top

    public struct MapTile
    {
        public int[,] contents;
        public bool[] entrances;
    }

    private MapTile[,] mTileGrid = new MapTile[5, 5];
    private int gridSize = 5; //Change this value with grid size
    private float numOfSprites = 10.0f;

    //Functions
    void Start()
    {
        //Create Tiles
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                mTileGrid[i,j] = CreateTile(mTileGrid[i, j], new Vector2(i, j));
            }
        }

        //Show Tiles
        float tileSize = spriteSize * numOfSprites;
        for (int i = 0; i < gridSize; i++)
        {
            for (int j = 0; j < gridSize; j++)
            {
                PrintTile(mTileGrid[i,j], new Vector3(i * tileSize , j * tileSize, 0.0f));
            }
        }
    }

    private MapTile CreateTile(MapTile tile, Vector2 coords)
    {
        tile.contents = new int[10, 10];

        tile = AssignEntrances(tile, coords);

        tile = CreateBoundary(tile);
        tile = PopulateTile(tile);

        return tile;
    }

    private MapTile AssignEntrances(MapTile tile, Vector2 coords)
    {
        float chanceOfEntrance = 50;

        //LEFT ENTRANCE
        if(coords.x != 0 && Random.Range(1,100) <= chanceOfEntrance)
        {
            tile.entrances[0] = true;
        }

        //Right
        if (coords.x != gridSize-1 && Random.Range(1, 100) <= chanceOfEntrance)
        {
            tile.entrances[1] = true;
        }

        //Bottom
        if (coords.y != 0 && Random.Range(1, 100) <= chanceOfEntrance)
        {
            tile.entrances[2] = true;
        }

        //Top
        if (coords.y != gridSize - 1 && Random.Range(1, 100) <= chanceOfEntrance)
        {
            tile.entrances[3] = true;
        }

        return tile;
    }

    private MapTile CreateBoundary(MapTile tile)
    {
        int tileLength = tile.contents.GetLength(0);
        // Left Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[0])
            {
                tile.contents[0, i] = GetRandomTileWeighted();
            }
            else
            {
                tile.contents[0, i] = 7;
            }
        }

        // Right Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[1])
            {
                tile.contents[tileLength - 1, i] = GetRandomTileWeighted();
            }
            else
            {
                tile.contents[tileLength - 1, i] = 7;
            }
        }

        // Top Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[2])
            {
                tile.contents[i, 0] = GetRandomTileWeighted();
            }
            else
            {
                tile.contents[i, 0] = 7;
            }
        }

        // Bottom Edge
        for (int i = 0; i < tileLength; i++)
        {
            if ((i == 4 || i == 5) && tile.entrances[3])
            {
                tile.contents[i, tileLength - 1] = GetRandomTileWeighted();
            }
            else
            {
                tile.contents[i, tileLength - 1] = 7;
            }
        }

        return tile;
    }

    private MapTile PopulateTile(MapTile tile)
    {
        int tileLength = tile.contents.GetLength(0);
        for (int i = 1; i < tileLength - 1; i++)
        {
            for (int j = 1; j < tileLength - 1; j++)
            {
                tile.contents[i, j] = GetRandomTileWeighted();
            }
        }

        return tile;
    }

    private int GetRandomTileWeighted()
    {
        float totalWeighting = SumArray(tileWeightings);

        float rand = Random.Range(0, 100) / 100.0f;

        int index = 0;
        float counter = 0.0f;
        foreach (float weighting in tileWeightings)
        {
            if ((counter + weighting) / totalWeighting >= rand)
            {
                return (index);
            }
            else
            {
                counter += weighting;
                index++;
            }
        }
        return (0);
    }
    public float SumArray(float[] toBeSummed)
    {
        float sum = 0;
        foreach (float item in toBeSummed)
        {
            sum += item;
        }
        return sum;
    }

    //////////// PRINTING TILES
    private void PrintTile(MapTile tile, Vector3 position)
    {
        GameObject tileObj = new GameObject("Tile");
        tileObj.transform.position = position;

        int tileLength = tile.contents.GetLength(0);

        for (int i = 0; i < tileLength; i++)
        {
            for (int j = 0; j < tileLength; j++)
            {
                PrintSprite(tile, tileObj, i, j);
            }
        }
    }
    
    private void PrintSprite(MapTile tile, GameObject parent, int xCoord, int yCoord)
    {
        GameObject subTileObj = new GameObject("SubTile");
        subTileObj.transform.parent = parent.transform;

        SpriteRenderer rend = subTileObj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        rend.sprite = tileSprites[tile.contents[xCoord, yCoord]];

        subTileObj.transform.localPosition = new Vector3(xCoord, yCoord, 0.0f) * spriteSize;
        subTileObj.transform.localScale = new Vector3(0.7955f, 0.7818f, 1.0f);
    }
}
